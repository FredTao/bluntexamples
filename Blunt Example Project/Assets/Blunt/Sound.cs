/*************************************************************************************

	blunt - a dull C# library - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios, jthorborg.com]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	See \licenses\ for additional details on licenses associated with this program.

*************************************************************************************/

using UnityEngine;
using System.Collections;

namespace Blunt
{

	namespace Sound
	{
		using DSPType = System.Single;


		public class AudioConfiguration
		{

			static private AudioConfiguration internalInstance;

			static public AudioConfiguration instance()
			{
				if(internalInstance == null)
				{
					internalInstance = new AudioConfiguration();
				}
				return internalInstance;
			}
			/// <summary>
			/// The interface for receiving notifications through AudioConfiguration
			/// </summary>
			public interface AudioNotification
			{
				void sampleRateChanged(double newSampleRate);
				void pitchChanged(double newPitch);
				void latencyChanged(int newLatency);

			}
			/// <summary>
			/// A base class for recieving notifications, that automatically adds and removes itself
			/// from AudioConfiguration.
			/// </summary>
			public class AudioEventListener : AudioNotification
			{
				public void releaseListener()
				{
					AudioConfiguration.instance ().removeNotification (this);
						
				}

				~AudioEventListener()
				{
					releaseListener();
				}
				public AudioEventListener()
				{
					AudioConfiguration.instance().addNotification(this);
				}

				public virtual void sampleRateChanged(double newSampleRate)
				{

				}
				public virtual void pitchChanged(double newPitch)
				{

				}

				public virtual void latencyChanged(int newLatency)
				{

				}
			}

			public void removeNotification(AudioNotification notif)
			{
				lock (soundListeners) 
				{
					soundListeners.Remove (notif);
				}
			}

			public void addNotification(AudioNotification notif)
			{
				lock (soundListeners) 
				{
					soundListeners.Add (notif);
				}
			}
			private System.Collections.Generic.List<AudioNotification> soundListeners;


			private AudioConfiguration()
			{
				soundListeners = new System.Collections.Generic.List<AudioNotification> ();
			}
		}

		/// <summary>
		/// Sound stage.
		/// 	The most basic component: They can generate or produce sound, in stages.
		/// </summary>
		public interface SoundStage /*: AudioConfiguration.AudioNotification*/
		{
			/// <summary>
			/// Process audio into the data buffer, which is interleaved with <channels> samples.
			/// Note that the calling thread is potentially arbitrary, do NOT mix this code
			/// with GUI code or do anything stupid.
			/// Also note that you should ONLY process nSamplesFrames * channels indices - the
			/// data buffer may be larger than this value.
			/// </summary>
			/// <param name="data">Data.</param>
			/// <param name="nSampleFrames">How many samples to process for each channel in this frame.</param>
			/// <param name="channels">Channels.</param>
			/// <param name="channelIsEmpty">If set to <c>true</c> channel is empty.</param>
			void process (float[] data, int nSampleFrames, int channels, bool channelIsEmpty);
		}

        public interface MonoFilter
        {
            FilterPlugin getFilter();
        }

        public interface FilterPlugin : SoundStage
        {
            void enable(bool doEnable);
            void setMix(float mix);
            void calculateCoefficients();
        }
	}
}