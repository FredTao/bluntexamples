﻿/*************************************************************************************

	blunt - a dull C# library - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios, jthorborg.com]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	See \licenses\ for additional details on licenses associated with this program.

*************************************************************************************/


//#define IMAGESYNTH_SWAP_BUFFERS_WHILE_RENDERING

using UnityEngine;
using System.Collections;
using Blunt.Sound;
using System;
using System.Collections.Generic;

using Blunt;



namespace Blunt.Synthesis
{

    using DSPType = System.Single;

    using SystemFloat = System.Single;

    public class ImageSynth : SequencingSynthesizer<ImageSynth>
    {

        public enum SynthesisMethod
        {
            Linear, Logarithmic, Harmonic
        };

        public SynthesisMethod synthesisMethod
        {
            get { return method; }
            set { method = value; }
        }

        private SynthesisMethod method;


        public const int bufferSize = 512;
        public int lengthAverage = 8;

        private Utils.CheapRandom ran = new Utils.CheapRandom();

        private class WaveBasis
        {
            public DSPType[] aLeft, bLeft, aRight, bRight;

            public DSPType phase, omega;

            public WaveBasis(DSPType[][] a, DSPType[][] b)
            {
                phase = omega = 0;
                aLeft = a[0];
                bLeft = b[0];
                aRight = a[1];
                bRight = b[1];
                
            }

            public void resetProgression(DSPType phaseOffset = 0, DSPType omegaStep = 0)
            {
                phase = phaseOffset;
                omega = omegaStep;
            }
        }

        bool widen = true;
        bool debug = true;
        public DSPType[][] 
            bufferOne = new DSPType[2][], 
            bufferTwo = new DSPType[2][], 
            bufferThree = new DSPType[2][];

        public CResizableContainer<float>[] phaseOffsets;


        WaveBasis sMat1, sMat2;


        float synthesisRate;
        float actualSynthTime;
        public float synthesisFps;
        float offsetPhase;

        public ImageSynth()
        {
            phaseOffsets = new CResizableContainer<float>[2];
            setCutThreshold(-110);
            numSamplesToCut = (int)EnvironmentTuning.sampleRate / 4;
            synthesisRate = 0;
            for(int i = 0; i < 2; ++i)
            {
                bufferOne[i] = new DSPType[bufferSize + 2];
                bufferTwo[i] = new DSPType[bufferSize + 2];
                bufferThree[i] = new DSPType[bufferSize + 2];
                phaseOffsets[i] = new CResizableContainer<float>();
            }

        }

        void updatePhaseTables(uint harmonics)
        {
            if(phaseOffsets[0].size() != harmonics)
            {
                phaseOffsets[0].ensureSize(harmonics);
                phaseOffsets[1].ensureSize(harmonics);

                for (int i = 0; i < harmonics; ++i)
                {
                    phaseOffsets[0][i] = ran.random01();
                    phaseOffsets[1][i] = ran.random01();
                }
            }

        }

        /// <summary>
        /// Synthesise the next block of material.
        /// Don't call this from the audio thread! It is slow! Must be called from the main thread.
        /// The rate of which you call this determines how fast the frames are interpolated.
        /// If swapAxis is false, the algorithm will measure intensity from the middle bottom to the top.
        /// Otherwise, it will measure intensity from the left middle to the right middle.
        /// </summary>
        /// <param name="image"></param>
        public void resynthesize(ImageSignalTransforms.ColourImageCoeffs1D coeffs)
        {
            unchecked
            {
                
                var sps = Time.time - synthesisRate;

                //Debug.Log("sps = " + sps);
                synthesisRate = Time.time;
                if (sMat1 != null && sMat1.phase < 1)
                    return;

                uint harmonics = (uint)coeffs.bufferSize;
                updatePhaseTables(harmonics);
                var ciRed = coeffs.red.getData();
                var ciGreen = coeffs.green.getData();
                var ciBlue = coeffs.blue.getData();

                var sineBuf = EnvironmentTuning.sineLut512;

                if (sineBuf.Length < bufferThree.Length)
                    Debug.Log("Warning! GlobalData.sineLut is shorter than it's supposed to be!");

                // scale


                const uint wrap = bufferSize - 1;

                Array.Clear(bufferThree[0], 0, bufferThree[0].Length);
                Array.Clear(bufferThree[1], 0, bufferThree[0].Length);



                // synthesis - candidate for ifft
                if(!widen)
                {
                    /*
                    if(debug)
                    {

                        for (uint n = 0; n < bufferThree[0].Length; ++n)
                        {

                            bufferThree[0][n] = 1;
                            bufferThree[1][n] = 1;
                        }
                    }
                    else
                    {
                        for (uint n = 0; n < bufferThree[0].Length; ++n)
                        {

                            bufferThree[0][n] = 0;
                            bufferThree[1][n] = 0;
                        }
                    }

                    debug = !debug;
                    */
                    
                    for (uint i = 0; i < harmonics; ++i)
                    {
                        float vLeft = (float)MathExt.confineTo(ciRed[i] - ciGreen[i], 0, 1);
                        float vRight = (float)MathExt.confineTo(ciGreen[i] - ciRed[i], 0, 1);

                        for (uint n = 0; n < bufferThree[0].Length; ++n)
                        {

                            bufferThree[0][n] += vLeft * sineBuf[n * i & wrap];
                            bufferThree[1][n] += vRight * sineBuf[n * i & wrap];
                        }
                    }
                }
                else
                {

                    for (uint i = 0; i < harmonics; ++i)
                    {
                        //float vLeft = Mathf.Clamp01(ci[i].r - ci[i].g);
                        // float vRight = Mathf.Clamp01(ci[i].g - ci[i].r);

                        float vLeft = (float)ciRed[i];
                        float vRight = (float)ciGreen[i];


                        float leftRotation = (float)(i + 1) + (int)(ciBlue[i] * 5 - 3);
                        float rightRotation = (float)(i + 1) + (int)(ciBlue[i] * 5 - 3);


                        vLeft /= leftRotation;
                        vRight /= rightRotation;

                        if (leftRotation < 1 || leftRotation > harmonics)
                            leftRotation = vLeft = 0;

                        if (rightRotation < 1 || rightRotation > harmonics)
                            rightRotation = vRight = 0;

                        float leftPhase = phaseOffsets[0][i] * bufferSize;
                        float rightPhase = phaseOffsets[1][i] * bufferSize;
                        int y;
                        float frac, mfrac, x1, x2, sample;
                        for (uint n = 0; n < bufferThree[0].Length; ++n)
                        {

                            y = (int)leftPhase;
                            frac = leftPhase - y;
                            mfrac = 1.0f - frac;


                            x1 = sineBuf[y];
                            x2 = sineBuf[y + 1];
                            sample = x1 * mfrac + x2 * frac;


                            bufferThree[0][n] += sample * vLeft;

                            y = (int)rightPhase;
                            frac = rightPhase - y;
                            mfrac = 1.0f - frac;

                            x1 = sineBuf[y];
                            x2 = sineBuf[y + 1];
                            sample = x1 * mfrac + x2 * frac;

                            bufferThree[1][n] += sample * vRight;

                            leftPhase += leftRotation;
                            leftPhase -= (int)(leftPhase / bufferSize) * bufferSize;
                            rightPhase += rightRotation;
                            rightPhase -= (int)(rightPhase / bufferSize) * bufferSize;
                        }
                    }
                }

                // swap buf1 with buf2, and buf2 with buf3
                // such that buf1 = buf2, and buf2 = buf3
                // note this wont affect any concurrent rendering
                var temp = bufferOne;
                bufferOne = bufferTwo;
                bufferTwo = bufferThree;
                bufferThree = temp;

                var material = new WaveBasis(bufferOne, bufferTwo);

                

                synthesisFps = 1.0f / (Time.time - actualSynthTime);
                actualSynthTime = Time.time;

                material.resetProgression(0, 1.0f / (EnvironmentTuning.sampleRate / (1.0f / (sps * 0.5f))));
                // material.resetProgression(0, 1.0f / (EnvironmentTuning.sampleRate / 60));
                // atomically assign the new set
                sMat1 = material;
            }
 
        }

        public class Voice : Synthesis.Voice
        {

            public DSPType frequency;

            public class Osc
            {
                public float phase, omega, volume;
                public Osc(float p, float o, float v) { phase = p; omega = o; volume = v; }
            }

            /// <summary>
            /// Relation is the rotation speed relative to the fundamental.
            /// You can use EnvironmentTuning.pitchMultiplierFromSemitone() for this.
            /// </summary>
            /// <param name="relation"></param>
            public void addOscillator(float relation, float volume = 1.0f)
            {
                oscillators.Add(new Osc(0, relation, volume));
            }

            public List<Osc> oscillators = new List<Osc>();

            ImageSynth hostSynth;

            public Voice(ImageSynth synth)
                : base(synth)
            {
                hostSynth = synth;
            }

            public override void setPitch(DSPType freq)
            {
                frequency = freq;
            }


            public override void play()
            {
                enabled = false;
                foreach (Osc o in oscillators)
                {
                    o.phase = 0;
                }
                timeAtPlay = (float)hostSynth.getPlayHead().timeInSeconds;
                timeSinceLastThreshold = 0;
                enabled = true;
                adsr.stop();
            }

            public override void play(float volumeInDBs)
            {
                this.volume = MathExt.dbToFraction(volumeInDBs);
                play();
            }

        }

        /// <summary>
        /// This defines the maximum latency of the system in samples - it may be lower.
        /// This is also effectively the maximum rate of which voices, operators and sequencing
        /// is triggered.
        /// </summary>
        private int latency = 1024;
        private List<Voice> voices = new List<Voice>();
        /// <summary>
        /// If a voice's output RMS for @latency frames is below this threshold, 
        /// it gets marked silent for a period of time.
        /// If this period exceeds @numSamplesToCut, the voice gets cut.
        /// </summary>
        private DSPType cutoffThreshold;
        /// <summary>
        /// See @cutoffThreshold.
        /// </summary>
        private int numSamplesToCut;
        // only for display!
        public int numVoices = 0;
        public int numActiveVoices = 0;
        public int numActiveOperators = 0;
        /// <summary>
        /// When using exponential decays, the magnitude of the numbers keep getting smaller
        /// until a point where they become denormal and virtually destroy performance.
        /// This threshold flushes numbers to zero if the become smaller.
        /// </summary>
        public float denormalFlush = MathExt.dbToFraction(-96)/*float.Epsilon*/;
        public float tempo = 0.025f;
        private PlayHead playHead = new PlayHead();

        private CResizableContainer<DSPType> envBuf = new CResizableContainer<DSPType>();
        private CResizableContainer<DSPType> voiceBuf = new CResizableContainer<DSPType>();

        public void setLatency(int newLatency)
        {
            latency = Mathf.NextPowerOfTwo(newLatency);
        }

        public PlayHead getPlayHead()
        {
            return playHead;
        }

        public void setCutThreshold(DSPType cutoffInDBs)
        {
            cutoffThreshold = (DSPType)Math.Abs(MathExt.dbToFraction(cutoffInDBs));
        }

        public Synthesis.Voice createVoice(int midiNote, DSPType detuneInCents = 0)
        {
            Voice v = new Voice(this);
            v.midiNote = midiNote;
            voices.Add(v);
            numVoices++;
            return v;
        }

        public Synthesis.Voice createVoice()
        {
            Voice v = new Voice(this);
            voices.Add(v);
            numVoices++;
            return v;
        }




        public void addVoice(Synthesis.Voice newVoice)
        {
            Voice v = newVoice as Voice;
            if (v != null)
            {
                var voicelist = new List<Voice>(voices);
                voicelist.Add(v);
                voices = voicelist;
            }
        }

        public void removeVoice(Synthesis.Voice newVoice)
        {
            Voice v = newVoice as Voice;
            if (v != null)
            {
                var voicelist = new List<Voice>(voices);
                voicelist.Remove(v);
                voices = voicelist;
            }
        }

        private List<SynthSequenceCallback<ImageSynth>> sequencers = new List<SynthSequenceCallback<ImageSynth>>();

        public void addSequencer(SynthSequenceCallback<ImageSynth> cb)
        {
            var seqs = new List<SynthSequenceCallback<ImageSynth>>(sequencers);
            seqs.Add(cb);
            // atomically change array
            sequencers = seqs;
        }
        public void removeSequencer(SynthSequenceCallback<ImageSynth> cb)
        {
            var seqs = new List<SynthSequenceCallback<ImageSynth>>(sequencers);
            seqs.Remove(cb);
            // atomically change array
            sequencers = seqs;
        }

        private void doSequencing(int nSamples)
        {
            var seqs = this.sequencers;

            for (int i = 0; i < seqs.Count; ++i)
            {
                seqs[i].onSequenceCallback(this, playHead);
            }

            playHead.advance(nSamples);
        }


        public void process(SystemFloat[] data, int nSampleFrames, int channels, bool channelIsEmpty)
        {
            //Debug.Log("im at " + GameState.audioPlayHead.nTotalSamples + ", own = " + playHead.nTotalSamples);
            doSequencing(nSampleFrames);
            var material = sMat1;
            if (material == null)
                return;
            var lbufA = material.aLeft;
            var lbufB = material.bLeft;
            var rbufA = material.aRight;
            var rbufB = material.bRight;


            float frac, mfrac, x1, x2, alsample, blsample, arsample, brsample, lsample, rsample;

            int y;

            envBuf.ensureSize(nSampleFrames);
            voiceBuf.ensureSize(nSampleFrames * channels);

            var vbuf = voiceBuf.getData();
            var ebuf = envBuf.getData();

            var activeVoices = 0;

            foreach (Voice v in voices)
            {
                if (!v.enabled)
                    continue;

                activeVoices++;
                
                Array.Clear(vbuf, 0, vbuf.Length);

                // generate envelope for this voice.
                v.adsr.processBatch2(ebuf, nSampleFrames);

                // the basic rotation
                float unityRotation = bufferSize * v.frequency * EnvironmentTuning.deltaT;


                foreach (Voice.Osc o in v.oscillators)
                {
                    float mphase = material.phase;
                    float momega = material.omega;

                    float ophase = o.phase;
                    float oomega = o.omega * unityRotation;

                    float ovol = o.volume;

                    for (int i = 0; i < data.Length; i += channels)
                    {


                        // help the conditional brancing logic
                        bool earlyComparison = (mphase + momega >= 1.0f);

                        // synthesis
                        y = (int)ophase;

                        frac = ophase - y;

                        mfrac = 1.0f - frac;

                        // interpolate sample from first buffer, left
                        x1 = lbufA[y];
                        x2 = lbufA[y + 1];
                        alsample = x1 * mfrac + x2 * frac;
                        // interpolate sample from second buffer, left.
                        x1 = lbufB[y];
                        x2 = lbufB[y + 1];
                        blsample = x1 * mfrac + x2 * frac;
                        // interpolate between buf1 and two
                        lsample = alsample * (1.0f - mphase) + blsample * mphase;

                        // interpolate sample from first buffer, right
                        x1 = rbufA[y];
                        x2 = rbufA[y + 1];
                        arsample = x1 * mfrac + x2 * frac;
                        // interpolate sample from second buffer, right
                        x1 = rbufB[y];
                        x2 = rbufB[y + 1];
                        brsample = x1 * mfrac + x2 * frac;
                        // interpolate between buf1 and two
                        rsample = arsample * (1.0f - mphase) + brsample * mphase;


                        if (earlyComparison)
                        {
                            mphase = 1.0f;
                            momega = 0.0f;
                        }


                        vbuf[i] += lsample * ovol;
                        vbuf[i + 1] += rsample * ovol;


                        ophase += oomega;
                        ophase -= (int)(ophase / (bufferSize)) * (bufferSize);

                        mphase += momega;
                    }

                    o.phase = ophase;

                }

                float averageOutput = 0, sample = 0, vvol = v.volume;
                for (int n = 0; n < nSampleFrames; n++)
                {

                    sample = vbuf[n * 2] * ebuf[n] * vvol;
                    averageOutput += sample * sample;
                    data[n * 2] += sample;
                    sample = vbuf[n * 2 + 1] * ebuf[n] * vvol;
                    data[n * 2 + 1] += sample;

                }

                ////////////////////////////////////////////////////////////
                // Cutting of voiceList.
                ////////////////////////////////////////////////////////////
                if (!v.dontKillOnSilence)
                {
                    averageOutput = Mathf.Sqrt((float)averageOutput / nSampleFrames);   // (number of samples this round).

                    if (cutoffThreshold > averageOutput)
                    {
                        v.timeSinceLastThreshold += nSampleFrames;
                        if (v.timeSinceLastThreshold > numSamplesToCut)
                        {
                            // cut the voice.
                            v.enabled = false;
                        }
                    }
                    else
                    {
                        // reset the timer, the voice lives on for a bit more!!
                        v.timeSinceLastThreshold = 0;
                    }
                }
            }

            material.phase += material.omega * nSampleFrames;
            if(material.phase >= 1.0f)
            {
                material.omega = 0.0f;
                material.phase = 1.0f;
            }
            numActiveVoices = activeVoices;

        }
    }
};